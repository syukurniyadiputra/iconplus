names = ['Ahmad', 'Tono', 'Tini', 'Bambang', 'Agus', 'Agung']

names.each do |name|
    # puts name.size 
    # puts name[0,1]
    # puts name.size.odd?
    if name.size.odd? == true && (name[0,1] == "A" || name[0,1] == "a")
        puts "nama sebelum #{name} menjadi #{name.upcase}"
    elsif name[0,1] == "T" || name[0,1] == "t"
        puts "nama sebelum #{name} menjadi #{name.downcase}"
    else
        name_down = name.downcase
        name_reverse = name_down.reverse
        puts "nama sebelum #{name} menjadi #{name_reverse.capitalize}"
    end
    #     puts name.uppercase
    # else 
    #     puts name
    # end 
    # puts name[0].chr
end