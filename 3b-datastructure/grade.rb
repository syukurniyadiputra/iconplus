h = {"Alice" => 75, "Bob" => 80, "Candra" => 100,"Dede" => 64,"Eka" => 90}

$nilai_rata = h.values.sum / h.length
$jmlh_bawah_rata = 0

def convert_value(nilai)
    if nilai >= 80
        return "A"
    elsif nilai >= 70 && nilai < 80
        return "B"
    elsif nilai >= 60 && nilai < 70
        return "C"
    elsif nilai >= 40 && nilai < 60
        return "D"
    elsif nilai < 40
        return "E"
    end
end

def rata_rata(h)
    h.each do |key,value|
        if value > $nilai_rata
            $jmlh_bawah_rata += 1
        end
    end

    puts "Jumlah Rata-rata nilai #{$nilai_rata} and jumlah nilai dibawah rata-rata #{$jmlh_bawah_rata}"
end

def list_grade_alphabet(h)
    h.each do |key,value|
        puts "nama : #{key}" 
        puts "grade : #{convert_value(value)}"
    end
end

rata_rata(h)
list_grade_alphabet(h)




